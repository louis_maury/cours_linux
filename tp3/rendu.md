# Rendu TP3 Linux:

## Questions:

- > *Connectez vous au host "proxy" avec vagrant et vérifier Comment est configuré le resolver dns du système ?*
- Le resolver est configuré ainsi: 
    ```shell
    vagrant@proxy:~$ cat /etc/resolv.conf 
    nameserver 192.168.33.21
    nameserver 192.168.33.22
    ```

---

- > *Retrouvez l'adresse ip du host wiki.lab.local avec la commande dig.*
- Pour trouver l'ip du host `wiki.lab.local` il faut faire comme ça:
    ```shell
    dig <leNomDuHost> +short
    ```
    Soit:
    ```shell
    vagrant@proxy:~$ dig wiki.lab.local +short
    192.168.56.11
    ```

---
- > *Connectez vous au host auth-1, quels sont les services réseaux qui sont en fonctionnement actuellement quels sont leur socket d'écoute ?*

- Il faut taper la commande `ss -luntp`. On peut donc voir en écoutes les processuces réseaux suivant:
    - `httpd`
    - `pdns_server`
    - `dhclient`

---

 - > *Connectez vous au host recursor-1,  quels sont les services réseaux qui sont en fonctionnement actuellement quels sont leur socket d'écoute ?*
 - Il faut taper la même commande que précedemment et on voit les precessuces suivants:
    - `pdns_recursor`
    - `dhclient`