# TP1

Sommaire:
- Explication du provisioning:
  - Le provisioning General
  - Le provisioning de `wiki`

Dans ce tp, il fallait faire un `Vagrantfile` qui crée deux VM Debian, un Wiki et une deuxième Backup.

Dans le provisioning il faut installer:
- `dokuwiki`
- `rsync`
- preparer la crontab pour faire un `rsync` de la wm **Wiki** à **backup**

## Explication du provisioning
### Le Povisionnig General

Voici le script:
```
cd ~
mkdir data
sudo apt update
sudo apt-get install apache2 libapache2-mod-php php-xml -y
sudo apt-get install php7.0 libapache2-mod-php7.0 php7.0-mcrypt -y
cd /var/www
sudo wget https://download.dokuwiki.org/src/dokuwiki/dokuwiki-stable.tgz
sudo tar xvf dokuwiki-stable.tgz
sudo mv dokuwiki-*/ dokuwiki
sudo chown -R www-data:www-data /var/www/dokuwiki
sudo vi -c :%s/html/dokuwiki -c :wq /etc/apache2/sites-enabled/000*.conf
sudo apt-get install ufw && sudo ufw allow 80 aa sudo ufw allow 443 && sudo ufw --force enable
sudo vi -c :172s/None/All -c :wq /etc/apache2/apache2.conf
sudo service apache2 restart
sudo apt-get install rsync
sudo apt update
```

Je vais expliquer ligne par ligne afin de rendre ça plus claire.
Tout d'abord, on crée le dossier `data` puis on fait un update pour pouvoir installer les prochaines applications.
On installe apache2 avec les modules php et xml puis une instalation de php7.0 se fait.
Une fois que c'est fait, on se deplace dans `/var/www/` puis on telecharge tranquillement `dokuwiki`, lui donne le droit de lecture dans le dossier par apache.
Une fois fait, la command `vi -c` permet d'effectuer une commande. Il faut donc tout simplement remplacer tous les mots `html` par `dokuwiki` dans les fichiers de configuration qui se trouvent dans le repertoire `/etc/apache2/sites-enabled/` *(bon quand je dis tous les mots `html`, il se trouve que `html` n'est ecrit qu'une fois dans le fichier...*
Maintenant avec `ufw` on va autoriser les ports 80 et 443 afin de pouvoir acceder au site via les requetes tcp (80->http, 443->htpps)
Bien !
Avec la commande `vi -c` on va remplacer le mot `None` par `All` dans le fichier `/etc/apache2/apache2.conf` mais on ne change que la ligne `172`.
On relance apache pour appliquer les changement sans reboot
On install `rsync` et on `update` tout ce beau petit monde et... **TADA** ! Le premier provisioning est **FINI**

Ce provisioning va se faire sur les deux vms.


Maitenant on peut attaquer la deuxième partie:
### Le Provisionning de `wiki`

`wiki` est la vm qui sera accessible depuis internet tandis que `backup` servira à (comme son nom l'indique) faire une backup si jamais la première avait un problème.
On va donc se mettre en root, et ajouter `5 * * * * root rsync ~/data/* wiki@192.168.57.3:~/data/` au fichier /etc/crontab,
relancer cron, se deconnecter de l'utilisateur root faire un uptime et dire que `wiki` est en marche.





